/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 *
 * @author Araujo
 */
public class Principal {
    
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("RelacionMuchosMuchosPU");
    private static EntityManager em = emf.createEntityManager();
    private static String idProducto, descripcion,idOrden,fecha,precio, monto;
    private static Integer id, cantidad,idorden,idproducto;
    private static Float prize;
    private static Collection<Producto> lista_producto;
    private static Collection<Orden> lista_orden;
    private static Collection<Lineaorden> lista_lineaorden;
    public static void main (String arg[]){
    
        Producto auxProducto;
        Orden auxOrden;
        Lineaorden auxlineaorden;
        LineaordenPK auxlineaordenpk;
        
       
        int op=0;
       
        do{
            op=Integer.parseInt(JOptionPane.showInputDialog("Manejo de Productos y Ordenes"+"\n"+
            "[1] Agregar un Producto"+"\n"+
            "[2] Agregar una Orden"+"\n"+
            "[3] Agregar un  Producto a una Orden"+"\n"+
            "[4] Eliminar Producto de una Orden"+"\n"+
            "[5] Actualizar un Producto"+"\n"+
            "[6] Actualizar fecha de una Orden"+"\n"+
            "[7] Lista de Productos"+"\n"+
            "[8] Lista de Ordenes"+"\n"+
            "[9] Lista de Ordenes con Productos (relaciones)"+"\n"+
            "[10] SALIR"+"\n"+
            "Ingresa una opcion:"));
            
            
 
            switch(op)
            {
                case 1:
                    descripcion=JOptionPane.showInputDialog("Ingresa la descripcion del Producto");
                    precio=JOptionPane.showInputDialog("Ingresa el precio del producto");
                      
                    try{
                      prize=new Float(precio);  
                      auxProducto=new Producto();
                      auxProducto.setIdProducto(idproducto);
                      auxProducto.setDescripcion(descripcion);
                      auxProducto.setPrecio(prize);
                      em.getTransaction().begin();
                      em.persist(auxProducto);
                      em.getTransaction().commit();
                      System.out.println("Se añadio correctamente el registro"+"\n"+
                                        "Id: "+idProducto+"\t"+"Nombre: "+descripcion+"\t"+precio);
                    }
                    catch(Exception e){
                      System.out.println(e);
                    }
                    
                    break;
                case 2:
                                     
                    fecha=JOptionPane.showInputDialog("Ingrese la fecha de la orden (dd-mm-aaaa)");
                        
                        try{
                          auxOrden= new Orden();
                          SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
                          Date Fecha = date.parse(fecha);
                          auxOrden.setFechaOrden(Fecha);
                          
                          em.getTransaction().begin();
                          em.persist(auxOrden);
                          em.getTransaction().commit();
                          
                            System.out.println(auxOrden.getIdOrden());
                          
                            
                        }
                        catch(Exception e){
                          System.out.println(e);
                        }
                      
                                      
                    break;
                    
                case 3:
                    
                    idOrden=JOptionPane.showInputDialog("Ingrese el ID de la Orden");
                    idorden=new Integer(idOrden);
                    
                    if(buscaOrden(idorden)){//Vefiica que la Orden exista
                      idProducto=JOptionPane.showInputDialog("Ingrese el ID del Producto");
                      idproducto=new Integer(idProducto);
                      
                      if(buscaProducto(idproducto)){//Verifica que el Producto exista
                          
                          auxlineaordenpk = new LineaordenPK(idorden,idproducto);
                          
                          if(buscaLineaOrden(auxlineaordenpk)){//Si ya existe el producto en esta orden 
                            auxlineaorden = em.find(Lineaorden.class,auxlineaordenpk);
                            JOptionPane.showMessageDialog(null,"Ya existe este producto en esta orden"+"\n"+
                                                          "ID orden: "+auxlineaorden.getOrden()+"\n"+"ID Producto: "+
                                                          auxlineaorden.getProducto().getDescripcion()+"\n"+"Cantidad:"+
                                                          auxlineaorden.getCantidad());
                          }
                          else{
                            monto=JOptionPane.showInputDialog("Escriba la cantidad a ingresar a la Orden");
                            
                            try{
                              cantidad= new Integer(monto);
                              
                              auxlineaorden = new Lineaorden();
                              auxlineaorden.setLineaordenPK(auxlineaordenpk);
                              auxlineaorden.setCantidad(cantidad);
                              
                              em.getTransaction().begin();
                              em.persist(auxlineaorden);
                              em.getTransaction().commit();
                                System.out.println("Se añadio la LineaOrden");
                            }
                            catch(Exception e){
                                System.out.println(e);
                            }
                          }
                        
                        
                      }
                      else{
                        JOptionPane.showMessageDialog(null,"No existe un producto con ese ID");
                      }
                    }
                    else{
                      JOptionPane.showMessageDialog(null,"No existe una orden con ese ID");
                    }
                    
                    break;
                    
                case 4:
                    
                    idOrden=JOptionPane.showInputDialog("Ingrese el ID de la Orden");
                    idorden=new Integer(idOrden);
                    
                    if(buscaOrden(idorden)){//Vefiica que la Orden exista
                      idProducto=JOptionPane.showInputDialog("Ingrese el ID del Producto");
                      idproducto=new Integer(idProducto);
                      
                      if(buscaProducto(idproducto)){//Verifica que el Producto exista
                          
                          auxlineaordenpk = new LineaordenPK(idorden,idproducto);
                          
                          if(buscaLineaOrden(auxlineaordenpk)){//Si ya existe el producto en esta orden 
                          
                              auxlineaorden=em.find(Lineaorden.class,auxlineaordenpk);
                              
                              int resp=JOptionPane.showConfirmDialog(null,"¿Estas seguro que deseas eliminar la siguiente LineaOrden? :"+"\n"+
                                      "ID de Orden: "+auxlineaorden.getOrden().getIdOrden()+"\n"+"Producto: "+auxlineaorden.getProducto().getDescripcion()+"\n"+
                                      "Cantidad: "+auxlineaorden.getCantidad());
                                if(resp==JOptionPane.YES_OPTION){
                                    try{
                                      em.getTransaction().begin();
                                      em.remove(auxlineaorden);
                                      em.getTransaction().commit();
                                      JOptionPane.showMessageDialog(null,"LineaOrden eliminado correctamente");
                                    }
                                    catch(Exception e){
                                      System.out.println(e);
                                    }
                                }
                                else{
                                  JOptionPane.showMessageDialog(null, "Se ha cancelado la operacion");
                                }
                          }
                          
                          else{
                            JOptionPane.showMessageDialog(null,"No existe un Producto registrado para esta Orden");
                
                          }
                        
                        
                      }
                      else{
                        JOptionPane.showMessageDialog(null,"No existe un producto con ese ID");
                      }
                    }
                    else{
                      JOptionPane.showMessageDialog(null,"No existe una orden con ese ID");
                    }
                    
                    break;
                    
                case 5:
                    
                    idProducto=JOptionPane.showInputDialog("Ingrese el ID del producto a actualizar");
                    idproducto=new Integer(idProducto);
                    
                    if(buscaProducto(idproducto)){
                      auxProducto=em.find(Producto.class, idproducto);
                      descripcion=JOptionPane.showInputDialog("Ingrese la nueva descripcion valor actual:"+auxProducto.getDescripcion());
                      precio=JOptionPane.showInputDialog("Ingrese el nuevo precio valor actual:"+auxProducto.getPrecio());
                      
                      try{
                        prize=new Float(precio);
                        
                        auxProducto.setDescripcion(descripcion);
                        auxProducto.setPrecio(prize);
                        
                        em.getTransaction().begin();
                        em.merge(auxProducto);
                        em.getTransaction().commit();
                      }
                      catch(Exception e){
                          System.out.println(e);
                      }
                    }
                    else{
                      JOptionPane.showMessageDialog(null,"No se encuentra el producto");
                    }
                    
                    break;
                case 7:
                    
                    try{
                      em.getTransaction().begin();
                      lista_producto=em.createNamedQuery("Producto.findAll").getResultList();           
                    
                      System.out.println("Lista de Productos:");
                      for(Producto producto: lista_producto){
                        System.out.println("ID: "+producto.getIdProducto()+"\n"+
                                           "Descripcion: "+producto.getDescripcion()+"\n"+
                                           "Precio: "+producto.getPrecio()+"\n");
                        
                      }                        

                      em.getTransaction().commit();
                    }
                    catch(Exception e){
                      System.out.println(e);
                    }
                    break;
                    
                    case 8:
                    
                    try{
                      em.getTransaction().begin();
                      lista_orden=em.createNamedQuery("Orden.findAll").getResultList();           
                    
                      System.out.println("Lista de Ordenes:");
                      for(Orden orden: lista_orden){
                        System.out.println("ID: "+orden.getIdOrden()+"\n"+
                                           "Fecha: "+orden.getFechaOrden()+"\n");
                        
                      }                        

                      em.getTransaction().commit();
                    }
                    catch(Exception e){
                      System.out.println(e);
                    }
                    break;
                    
                    case 9:
                    
                    try{
                      em.getTransaction().begin();
                      lista_lineaorden=em.createNamedQuery("Lineaorden.findAll").getResultList();           
                    
                      System.out.println("Lista de Ordenes y Productos:");
                      for(Lineaorden lineaorden: lista_lineaorden){
                        System.out.println("ID Orden: "+lineaorden.getOrden().getIdOrden()+"\n"+
                                           "Producto: "+lineaorden.getProducto().getDescripcion()+"\n"+
                                           "Cantidad: "+lineaorden.getCantidad()+"\n");
                        
                      }                        

                      em.getTransaction().commit();
                    }
                    catch(Exception e){
                      System.out.println(e);
                    }
                    break;
                     
            }
        }while(op!=10);
    
        //em.getTransaction().commit();
        
        em.close();
        emf.close();
    }
    
    public static boolean buscaProducto(Integer idProducto){
        Producto aux = null;
        aux = em.find(Producto.class, idProducto);
        
        if(aux==null){
        return false;
        }
        else{
        return true;
        }
    }
    
    public static boolean buscaOrden(Integer idOrden){
        Orden aux = null;
        aux = em.find(Orden.class, idOrden);
        
        if(aux==null){
        return false;
        }
        else{
        return true;
        }
    }
    
    public static boolean buscaLineaOrden(LineaordenPK lineaordenPK){
        Lineaorden aux = null;
        aux = em.find(Lineaorden.class,lineaordenPK);
        
        if(aux==null){
        return false;
        }
        else{
        return true;
        }
    }
}
